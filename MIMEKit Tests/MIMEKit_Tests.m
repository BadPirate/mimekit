//
//  MIMEKit_Tests.m
//  MIMEKit Tests
//
//  Created by Kevin Lohman on 4/7/15.
//  Copyright (c) 2015 Portable Knowledge, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "PKMIMEMessage.h"

@interface MIMEKit_Tests : XCTestCase

@end

@implementation MIMEKit_Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (NSBundle *)bundle {
    return [NSBundle bundleForClass:[self class]];
}

- (void)testMicrosoftSample {
    NSString *string = [NSString stringWithContentsOfURL:[self.bundle URLForResource:@"microsoft" withExtension:@"mime"]
                                                encoding:NSUTF8StringEncoding
                                                   error:nil];
    XCTAssert(string != nil);
    PKMIMEData *data = [PKMIMEData dataFromStringWithHeaders:string];
    PKMIMEMessage *message = [PKMIMEMessage messageWithData:data];
    XCTAssert([message.headers[@"from"] isEqualToString:@"John Doe <example@example.com>"], @"Invalid From - %@",message.headers[@"from"]);
}
@end
